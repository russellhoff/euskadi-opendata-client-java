package com.joninx.odeclient.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Datos de restaurantes, sidrerías y bodegas de Euskadi.
 * 
 * @author sanch
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompTranspDTO implements Serializable {
	
	private static final long serialVersionUID = -1302777379354199958L;
	
	private String documentName;
	private String templateType;
	private String locality;
	private String accesibility;
	private String accesibilityIconURL;
	private String accesibilityIconDescription;
	private String phone;
	private String address;
	private String marks;
	private String physical;
	private String visual;
	private String auditive;
	private String intellectual;
	private String organic;
	private String tourismEmail;
	private String web;
	private String postalCode;
	private String transportType;
	private String latitudelongitude;
	private String latwgs84;
	private String lonwgs84;
	private String municipality;
	private String municipalitycode;
	private String territory;
	private String territorycode;
	private String country;
	private String countrycode;
	private String friendlyUrl;
	private String physicalUrl;
	private String dataXML;
	private String metadataXML;
	private String zipFile;
	
}
