package com.joninx.odeclient.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Datos de campings de Euskadi.
 * 
 * @author sanch
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CampingDTO implements Serializable {
	
	private static final long serialVersionUID = -1302777379354199958L;

	private String documentName;
	private String turismDescription;
	private String templateType;
	private String phone;
	private String address;
	private String marks;
	private String physical;
	private String visual;
	private String auditive;
	private String intellectual;
	private String organic;
	private String tourismEmail;
	private String web;
	private String lodgingType;
	private String category;
	private String signatura;
	private String store;
	private String capacity;
	private String postalCode;
	private String latitudelongitude;
	private String latwgs84;
	private String lonwgs84;
	private String municipality;
	private String municipalitycode;
	private String territory;
	private String territorycode;
	private String country;
	private String countrycode;
	private String friendlyUrl;
	private String physicalUrl;
	private String dataXML;
	private String metadataXML;
	private String zipFile;
	
}
