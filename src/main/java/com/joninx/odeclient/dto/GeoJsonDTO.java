package com.joninx.odeclient.dto;

import java.io.Serializable;

import org.wololo.geojson.FeatureCollection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder;

/**
 * Clase que engloba datos en formato GeoJSON.
 * 
 * @author sanch
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GeoJsonDTO implements Serializable {

	private static final long serialVersionUID = 2389716827222517638L;

	private FeatureCollection geoObjets;
	
}
