package com.joninx.odeclient.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import lombok.Data;

@Service
@ConfigurationProperties(prefix="ode")
@Data
public class RestPropsService {

	private String base;
	private String reassibobapiJson;
	private String reassibobapiGeojson;
		
}
