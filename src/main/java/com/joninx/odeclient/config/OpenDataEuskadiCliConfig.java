package com.joninx.odeclient.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.joninx.odeclient.dto.GeoJsonDTO;
import com.joninx.odeclient.helper.GeoJsonDeserializer;

@Configuration
public class OpenDataEuskadiCliConfig {
	
	@Bean
	public Jackson2ObjectMapperBuilder objectMapperBuilder(GeoJsonDeserializer pGeoJsonDeserializer) {
	    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
//	    builder.serializationInclusion(JsonInclude.Include.NON_NULL);
	    builder.deserializerByType(GeoJsonDTO.class, pGeoJsonDeserializer);
	    return builder;
	}
	
}
