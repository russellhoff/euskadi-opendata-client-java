package com.joninx.odeclient.helper;

import java.io.IOException;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.joninx.odeclient.dto.GeoJsonDTO;

import org.springframework.boot.jackson.JsonComponent;
import org.wololo.geojson.FeatureCollection;

@JsonComponent
public class GeoJsonDeserializer extends JsonDeserializer<GeoJsonDTO> {

	@Override
	public GeoJsonDTO deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		FeatureCollection fc = null;
		JsonNode rootNode = p.getCodec().readTree(p);
		Iterator<JsonNode> it = rootNode.iterator();
		while(it.hasNext()) {
			JsonNode node = it.next();
			/*String unescapedString = StringEscapeUtils.unescapeJava(node.toString());
			String json = unescapedString.substring(0, unescapedString.length());*/
			String json = node.toString();
			fc = (FeatureCollection) org.wololo.geojson.GeoJSONFactory.create(json);
		}
		return GeoJsonDTO.builder()
				.geoObjets(fc)
				.build();
	}

}
