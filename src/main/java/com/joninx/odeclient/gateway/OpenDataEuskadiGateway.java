package com.joninx.odeclient.gateway;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.joninx.odeclient.dto.CampingDTO;
import com.joninx.odeclient.dto.CompTranspDTO;
import com.joninx.odeclient.dto.GeoJsonDTO;
import com.joninx.odeclient.dto.RestSidrBodDTO;
import com.joninx.odeclient.retrofit.CampingsService;
import com.joninx.odeclient.retrofit.CompTranspService;
import com.joninx.odeclient.retrofit.ResSidrBodService;
import com.joninx.odeclient.service.RestPropsService;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Service
public class OpenDataEuskadiGateway {

	private ObjectMapper objectMapper;
	private ResSidrBodService resSidrBodservice;
	private Retrofit retrofitMethods;
	private RestPropsService restPropsService;
	private CompTranspService compTranspService;
	private CampingsService campingsService;
	
	@Autowired
	public OpenDataEuskadiGateway(ObjectMapper pObjectMapper, RestPropsService pRestPropsService) {
		objectMapper = pObjectMapper;
		restPropsService = pRestPropsService;
		
		JacksonConverterFactory jcf = JacksonConverterFactory.create(objectMapper);
				
		final OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {

			@Override
			public Response intercept(Chain chain) throws IOException {
				String bodyRest = "";
				Request request = chain.request();
		        okhttp3.Response response = chain.proceed(request);
		        if(response.code() == 200) {
		            ResponseBody rb = response.body();
		            InputStream is = rb.byteStream();

		            BufferedReader br = null;
		            StringBuilder sb = new StringBuilder();

		            String line;
		            try {

		                br = new BufferedReader(new InputStreamReader(is));
		                while ((line = br.readLine()) != null) {
		                    sb.append(line);
		                }

		            } catch (IOException e) {
		                e.printStackTrace();
		            } finally {
		                if (br != null) {
		                    try {
		                        br.close();
		                    } catch (IOException e) {
		                        e.printStackTrace();
		                    }
		                }
		            }

		            String body = sb.toString();
		            int indexStart = body.indexOf("(");
		            int indexEnd = body.lastIndexOf(")");
		            bodyRest = body.substring(indexStart+1, indexEnd);
		            if(!bodyRest.startsWith("[")) {
		            	bodyRest = "[" + bodyRest + "]";
		            }
		            return response.newBuilder()
			        		.body(ResponseBody.create(okhttp3.MediaType.get("application/json"), bodyRest))
			        		.build();
		        } 
		        return response;
			}
		
		}).build();
		
		retrofitMethods = new Retrofit.Builder()
			    .baseUrl(restPropsService.getBase())
			    .client(client)
			    .addConverterFactory(jcf)
			    .build();

		resSidrBodservice = retrofitMethods.create(ResSidrBodService.class);
		compTranspService = retrofitMethods.create(CompTranspService.class);
		campingsService = retrofitMethods.create(CampingsService.class);
	}
	
	public void getResSidrBod(Callback<List<RestSidrBodDTO>> pCallback) {
		Call<List<RestSidrBodDTO>> ret = resSidrBodservice.getReAsSiBoBaPiList();
		ret.enqueue(pCallback);
	}

	public void getResSidrBodGeoJson(Callback<GeoJsonDTO> pCallback) {
		Call<GeoJsonDTO> ret = resSidrBodservice.getReAsSiBoBaPiGeoJson();
		ret.enqueue(pCallback);
	}

	public void getCompTransp(Callback<List<CompTranspDTO>> pCallback) {
		Call<List<CompTranspDTO>> ret = compTranspService.getCompTranspList();
		ret.enqueue(pCallback);
	}

	public void getCompTranspGeoJson(Callback<GeoJsonDTO> pCallback) {
		Call<GeoJsonDTO> ret = compTranspService.getCompTranspGeoJson();
		ret.enqueue(pCallback);
	}

	public void getCampings(Callback<List<CampingDTO>> pCallback) {
		Call<List<CampingDTO>> ret = campingsService.getCampingsList();
		ret.enqueue(pCallback);
	}

	public void getCampingsGeoJson(Callback<GeoJsonDTO> pCallback) {
		Call<GeoJsonDTO> ret = campingsService.getCampingsGeoJson();
		ret.enqueue(pCallback);
	}
	
}
