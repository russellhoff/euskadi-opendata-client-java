package com.joninx.odeclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EuskadiOpendataClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(EuskadiOpendataClientApplication.class, args);
	}

}

