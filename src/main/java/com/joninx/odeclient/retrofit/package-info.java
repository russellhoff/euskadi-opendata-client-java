/**
 * @author Jon Inazio (captain06@gmail.com)
 * 
 * Retrofit related classes
 */
package com.joninx.odeclient.retrofit;