package com.joninx.odeclient.retrofit;

import java.util.List;

import com.joninx.odeclient.dto.GeoJsonDTO;
import com.joninx.odeclient.dto.CompTranspDTO;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CompTranspService {
		
	@GET("contenidos/ds_recursos_turisticos/empresas_transporte_euskadi/opendata/transporte.json")
	Call<List<CompTranspDTO>> getCompTranspList();

	@GET("contenidos/ds_recursos_turisticos/empresas_transporte_euskadi/opendata/transporte.geojson")
	Call<GeoJsonDTO> getCompTranspGeoJson();
	
}
