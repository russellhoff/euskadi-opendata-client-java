package com.joninx.odeclient.retrofit;

import java.util.List;

import com.joninx.odeclient.dto.GeoJsonDTO;
import com.joninx.odeclient.dto.CampingDTO;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CampingsService {
		
	@GET("contenidos/ds_recursos_turisticos/campings_de_euskadi/opendata/alojamientos.json")
	Call<List<CampingDTO>> getCampingsList();

	@GET("contenidos/ds_recursos_turisticos/campings_de_euskadi/opendata/alojamientos.geojson")
	Call<GeoJsonDTO> getCampingsGeoJson();
	
}
