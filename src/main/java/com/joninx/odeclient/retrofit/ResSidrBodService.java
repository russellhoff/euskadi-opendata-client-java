package com.joninx.odeclient.retrofit;

import java.util.List;

import com.joninx.odeclient.dto.GeoJsonDTO;
import com.joninx.odeclient.dto.RestSidrBodDTO;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ResSidrBodService {
		
	@GET("contenidos/ds_recursos_turisticos/restaurantes_sidrerias_bodegas/opendata/restaurantes.json")
	Call<List<RestSidrBodDTO>> getReAsSiBoBaPiList();

	@GET("contenidos/ds_recursos_turisticos/restaurantes_sidrerias_bodegas/opendata/restaurantes.geojson")
	Call<GeoJsonDTO> getReAsSiBoBaPiGeoJson();
	
}
