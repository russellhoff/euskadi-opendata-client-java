package com.joninx.odeclient.bootstrap;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.joninx.odeclient.dto.CampingDTO;
import com.joninx.odeclient.gateway.OpenDataEuskadiGateway;

import lombok.extern.slf4j.Slf4j;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Slf4j
@Component
public class Init implements CommandLineRunner, Callback<List<CampingDTO>>  {

	@Autowired private OpenDataEuskadiGateway odeClient;
	
	@Override
	public void run(String... args) throws Exception {
//		odeClient.getResSidrBodGeoJson(this);
//		odeClient.getResSidrBod(this);
//		odeClient.getCompTransp(this);
		odeClient.getCampings(this);
	}

	@Override
	public void onResponse(Call<List<CampingDTO>> call, Response<List<CampingDTO>> response) {
		if(response.isSuccessful()) {
			log.debug("Response OK (HTTP " + response.code() + ").");
			for(CampingDTO comp : response.body()) {
				log.debug(comp.toString());
			}
		}else {
			try {
				log.error("Response unsuccessful (HTTP " + response.code() + "): " + response.errorBody().string());
			} catch (IOException e) {
				log.error("Error IOException", e);
			}
		}		
	}

	@Override
	public void onFailure(Call<List<CampingDTO>> call, Throwable t) {
		log.error("Failure.", t);
	}
	
}
